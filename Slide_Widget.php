<?php
/**
 * Makes a custom Widget for displaying content postd, custome post type available with genthemes
 *
 * Learn more: http://demo.genthemes.net/
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Slide_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	 
	public $slugwidget = 'Slide_Widget';   
	public $col = '';
	 
	function Slide_Widget() {
		$widget_ops = array( 'classname' => 'Slides Widget Display', 'description' => __( 'Use this widget to list your recent Aside, Status, Quote, and Link posts', 'genthemesv1' ) );
		$this->WP_Widget( $this->slugwidget, __('GT Slides Widget Display', 'genthemesv1' ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
		//add_action( 'wp_footer', array(&$this, 'slide_widget_display_scripts_print' ));
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP );
	
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( '', 'genthemesv1' ) : $instance['title'], $instance, $this->id_base);
		$type = apply_filters( 'widget_type', empty( $instance['type'] ) ? __( '', 'genthemesv1' ) : $instance['type'], $instance, $this->id_base);
		$post_type = apply_filters( 'widget_post_type', empty( $instance['post_type'] ) ? __( '', 'genthemesv1' ) : $instance['post_type'], $instance, $this->id_base);
		$taxonomy = apply_filters( 'widget_taxonomy', empty( $instance['taxonomy'] ) ? __( '', 'genthemesv1' ) : $instance['taxonomy'], $instance, $this->id_base);
		$resize_image = apply_filters( 'widget_resize_image', empty( $instance['resize_image'] ) ? __( '', 'genthemesv1' ) : $instance['resize_image'], $instance, $this->id_base);
		
		$transition = apply_filters( 'widget_transition', empty( $instance['transition'] ) ? __( '', 'genthemesv1' ) : $instance['transition'], $instance, $this->id_base);
		$speed = apply_filters( 'widget_transition', empty( $instance['speed'] ) ? __( '', 'genthemesv1' ) : $instance['speed'], $instance, $this->id_base);
		$caption = apply_filters( 'widget_caption', empty( $instance['caption'] ) ? __( '', 'genthemesv1' ) : $instance['caption'], $instance, $this->id_base);
		$pager = apply_filters( 'widget_pager', empty( $instance['pager'] ) ? __( '', 'genthemesv1' ) : $instance['pager'], $instance, $this->id_base);
		$auto = apply_filters( 'widget_auto', empty( $instance['auto'] ) ? __( '', 'genthemesv1' ) : $instance['auto'], $instance, $this->id_base);
		
		if ( ! isset( $instance['number'] ) )
			$instance['number'] = '10';
		if ( ! $number = absint( $instance['number'] ) )
 			$number = 10;
 			 
 		if ( ! isset( $instance['pager'] ) )
			$instance['pager'] = 'true';
		if ( ! $pager = strip_tags( $instance['pager'] ) )
 			$pager = 'true';
 			
 		if ( ! isset( $instance['auto'] ) )
			$instance['auto'] = 'true';
		if ( ! $auto = strip_tags( $instance['auto'] ) )
 			$auto = 'true';
 		/*
 		if ( ! isset( $instance['taxonomy'] ) )
			$instance['taxonomy'] = 'category';
		if ( ! $taxonomy = strip_tags( $instance['taxonomy'] ) )
 			$auto = 'category';*/
 			 
  
 			  ?> 
				
      <section class="box-slide-gt"> 
        <h2><small><?php echo $title;?></small></h2> 
         <ul class="" id="demo<?php echo $args['widget_id']; ?>">
         <?php		
         			$tax = null;
         			if($taxonomy == "category"){
         				$tax = "category_name";
         			} else {
         				$tax = $taxonomy;
         			}
					$query = array(
						'post_type' => $post_type,
						 $tax =>  $type, 
						'order' => 'DESC',
						'posts_per_page' => $number
						);
						$pageposts = new WP_Query($query); 
						while ( $pageposts->have_posts() ) :
								$pageposts->the_post();
								$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
								$featured_image = $featured_image_array[0];
						?>
            <li>
              <a href="<?php echo get_permalink();?>">
              <?php 
						$default_attr = array( 
						'class'	=> "attachment",
						'alt'	=> trim(strip_tags(get_the_excerpt())),
						'title'	=> trim(strip_tags( get_the_title())),
						);
						?>
              <?php 
							if ( has_post_thumbnail()) {
							   echo get_the_post_thumbnail(get_the_ID(), $resize_image, $default_attr); 
							}
							  
				?>
              </a>
            </li>
         <?php endwhile; ?> 
          </ul>   
		</section>
		
		<script>
				$(function() {
					var demo1 = $("#demo<?php echo $args['widget_id']; ?>").slippry({
						transition: '<?php echo $transition?>',
						useCSS: true,
						speed: <?php echo $speed?>,
						pause: 3000,
						captions: '<?php echo $caption?>',
						auto: <?php echo $auto;?>,
						pager: <?php echo $pager;?>,
						preload: 'visible'
					}); 
				});
			</script> 
 			<?php   
			echo $after_widget;

			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata();

			// end check for ephemeral posts
		 

			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['type'] = strip_tags( $new_instance['type'] );
		$instance['post_type'] = strip_tags( $new_instance['post_type'] );
		$instance['taxonomy'] = strip_tags( $new_instance['taxonomy'] ); 
		$instance['resize_image'] = strip_tags($new_instance['resize_image']);
		
		$instance['transition'] = strip_tags($new_instance['transition']);
		$instance['speed'] = (int) $new_instance['speed'];
		$instance['caption'] = strip_tags($new_instance['caption']);
		$instance['pager'] = strip_tags($new_instance['pager']);
		$instance['auto'] = strip_tags($new_instance['auto']);
		$instance['number'] = (int) $new_instance['number'];
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}
	 

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {
		$title = isset( $instance['title']) ? esc_attr( $instance['title'] ) : '';
		$type = isset( $instance['type']) ? esc_attr( $instance['type'] ) : '';
		$post_type = isset( $instance['post_type']) ? esc_attr( $instance['post_type'] ) : '';
		$taxonomy = isset( $instance['taxonomy']) ? esc_attr( $instance['taxonomy'] ) : 'category'; 
		$resize_image = isset( $instance['resize_image']) ? esc_attr( $instance['resize_image'] ) : '';
		$transition = isset( $instance['transition']) ? esc_attr( $instance['transition'] ) : '';
		$speed = isset( $instance['speed'] ) ? absint( $instance['speed'] ) : 1000;
		$caption = isset( $instance['caption']) ? esc_attr( $instance['caption'] ) : 'overlay';
		$pager = isset( $instance['pager']) ? esc_attr( $instance['pager'] ) : 'true';
		$auto = isset( $instance['auto']) ? esc_attr( $instance['auto'] ) : 'true';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 10;
?>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'genthemesv1' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'post_type' ) ); ?>"><?php _e( 'Post Type:', 'genthemesv1' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'post_type' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'post_type' ) ); ?>" >
			<?php 
				$post_types = get_post_types( '', 'names' ); 
				$count = count($terms);
				foreach ( $post_types as $post_types ) {
				
				if($post_types == 'page' || $post_types == 'attachment' || $post_types == 'revision' 
					|| $post_types == 'nav_menu_item' || $post_types == 'template'){
					false;	
				}else{
				?>
				 <option value="<?php echo $post_types ?>" <?php selected($post_type, $post_types); ?>>
				  <?php echo $post_types; ?>
				 </option> 
				 <?php 
				}}
			?>
			</select>
			</p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'taxonomy' ) ); ?>"><?php _e( 'Taxonomy:', 'genthemesv1' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'taxonomy' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'taxonomy' ) ); ?>" >
			<?php 
				$taxonomies=get_taxonomies('','names');   
				foreach ( $taxonomies as $post_taxonomy ) { 
					
					if($post_taxonomy == 'post_tag' || $post_taxonomy == 'nav_menu' || $post_taxonomy == 'link_category' 
					|| $post_taxonomy == 'post_format'){
					false;	
				}else{
				       ?>
				       <option value="<?php echo $post_taxonomy ?>" <?php selected($taxonomy, $post_taxonomy); ?>>
				       <?php echo $post_taxonomy; ?>
				       </option> 
				        <?php
				} 
				}
			?>
			</select>
			</p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>"><?php _e( 'Category:', 'genthemesv1' ); ?></label>
	  		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'type' ) ); ?>" type="text" value="<?php echo esc_attr( $type ); ?>" /></p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'resize_image' ) ); ?>"><?php _e( 'Resize Image:', 'genthemesv1' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'resize_image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'resize_image' ) ); ?>" >
				<option value="thumbnail" <?php selected($resize_image,'thumbnail')?>>Thumbnail</option>
				<option value="medium" <?php selected($resize_image,'medium')?>>Medium</option>
				<option value="large" <?php selected($resize_image,'large')?>>Large</option>
				<option value="full" <?php selected($resize_image,'full')?>>Full</option>
			</select>
			</p>
			
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'transition' ) ); ?>"><?php _e( 'Transition:', 'genthemesv1' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'transition' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'transition' ) ); ?>" >
				<option value="fade" <?php selected($transition,'fade')?>>Fade</option>
				<option value="horizontal" <?php selected($transition,'horizontal')?>>Horizontal</option>
				<option value="vertical" <?php selected($transition,'vertical')?>>Vertical</option>
				<option value="kenburns" <?php selected($transition,'kenburns')?>>Kenburns</option>
			</select>
			</p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'speed' ) ); ?>"><?php _e( 'Speed:', 'genthemesv1' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'speed' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'speed' ) ); ?>" type="text" value="<?php echo esc_attr( $speed ); ?>"  /></p>
			
 			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'Caption' ) ); ?>"><?php _e( 'Caption:', 'genthemesv1' ); ?></label>
				<input type="radio" value="overlay" id="<?php echo esc_attr( $this->get_field_id( 'caption' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'caption' ) ); ?>" <?php checked($caption, 'overlay');?>>
				<label for="<?php echo esc_attr( $this->get_field_id( 'overlay' ) ); ?>"><?php _e( 'overlay', 'genthemesv1' ); ?></label>
				
				<input type="radio" value="false" id="<?php echo esc_attr( $this->get_field_id( 'caption' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'caption' ) ); ?>" <?php checked($caption, 'false');?>>
				<label for="<?php echo esc_attr( $this->get_field_id( 'false' ) ); ?>"><?php _e( 'false', 'genthemesv1' ); ?></label>
			</p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'Pager' ) ); ?>"><?php _e( 'Pager:', 'genthemesv1' ); ?></label>
				<input type="radio" value="true" id="<?php echo esc_attr( $this->get_field_id( 'pager' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'pager' ) ); ?>" <?php checked($pager, 'true');?>>
				<label for="<?php echo esc_attr( $this->get_field_id( 'true' ) ); ?>"><?php _e( 'true', 'genthemesv1' ); ?></label>
				
				<input type="radio" value="false" id="<?php echo esc_attr( $this->get_field_id( 'pager' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'pager' ) ); ?>" <?php checked($pager, 'false');?>>
				<label for="<?php echo esc_attr( $this->get_field_id( 'false' ) ); ?>"><?php _e( 'false', 'genthemesv1' ); ?></label>
			</p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'Auto' ) ); ?>"><?php _e( 'Auto Play:', 'genthemesv1' ); ?></label>
				<input type="radio" value="true" id="<?php echo esc_attr( $this->get_field_id( 'auto' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'auto' ) ); ?>" <?php checked($auto, 'true');?>>
				<label for="<?php echo esc_attr( $this->get_field_id( 'true' ) ); ?>"><?php _e( 'true', 'genthemesv1' ); ?></label>
				
				<input type="radio" value="false" id="<?php echo esc_attr( $this->get_field_id( 'auto' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'auto' ) ); ?>" <?php checked($auto, 'false');?>>
				<label for="<?php echo esc_attr( $this->get_field_id( 'false' ) ); ?>"><?php _e( 'false', 'genthemesv1' ); ?></label>
			</p>
			 
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'genthemesv1' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Slide_Widget");') );

function Slide_Widget_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Slide_Widget_scripts');