<?php
/*
Plugin Name: Slides Widget Display
Description: Slides Widget Post Content, Custome Post type
Version: 1.0.1
Author: Ade Iskandar
Author URI: http://genthemes.net
Email: berbagimadani@gmail.com
*/
?>
<?php
define('SLIDE_WIDGET_DISPLAY_FOLDER', dirname(plugin_basename(__FILE__))); 
define('SLIDE_WIDGET_DISPLAY_PLUGIN_NAME', plugin_basename(__FILE__) );  
require_once( plugin_dir_path( __FILE__ ). 'Slide_Widget.php'); 
add_action( 'wp_enqueue_scripts', 'slide_widget_display_scripts');
function slide_widget_display_scripts(){ 
    wp_enqueue_style('slippry-css', plugins_url( '/css/slippry.css', __FILE__)); 
    wp_enqueue_script('jquery-1-7-2', plugins_url( '/js/jquery-1.7.2.min.js',  __FILE__  ), false);
    wp_enqueue_script('slippry-js', plugins_url( '/js/slippry.js' , __FILE__ ), false);	  
} 
register_activation_hook(__FILE__,'slide_widget_display_install');
function slide_widget_display_install(){ 
	  add_option("slide_widget_display_options", '', '', 'yes'); 
}

register_deactivation_hook( __FILE__, 'slide_widget_display_remove' );
function slide_widget_display_remove() { 
	delete_option('slide_widget_display_options');
} 

?>